variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "nopnithi1@a.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "914314489486.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-devops-1:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "914314489486.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-proxy-1:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
