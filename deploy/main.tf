terraform {
  required_version = ">= 1.1.8"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.8.0"
    }
  }

  backend "s3" {
    bucket         = "nopnithi-recipe-app-api-devops-tfstate-1"
    key            = "recipe-app.tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock-1"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
